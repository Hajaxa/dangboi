#ifndef KEY_H
#define KEY_H

#include <QLayout>
#include <QPushButton>
#include <QDebug>
#include <qt_windows.h>

#include "fancyslider.h"

typedef struct  s_BtnTools
{
    QPushButton *btnDel;
    QPushButton *btnSong;
    QPushButton *btnBind;
}               t_BtnTools;

class Key
{
public:
    Key();
    Key(int _id, QString &_pathSong);
    Key(int _id, QHBoxLayout *_topLayout, s_BtnTools _tools, QPushButton *btn, FancySlider *_volume, QString &_pathSong, QString &_bindedKey);
    ~Key();

    void        setId(int _id);
    void        setTopLayout(QHBoxLayout *_topLayout);
    void        setTools(QPushButton *del, QPushButton *song, QPushButton *bind);
    void        setBtn(QPushButton *btn);
    void        setBtn();
    void        setDialVolume(FancySlider *_volume);
    void        setPathSong(QString path);
    void        setBindedKey(QString _bindedKey, DWORD _bindedKeyCode);
    void        setVolume(int vol);
    void        setBtnStyle(QString iconPath);

    int         getId();
    QHBoxLayout *getTopLayout();
    s_BtnTools  getTools();
    QPushButton *getBtn();
    FancySlider *getDial();
    QString     getPathSong();
    QString     getBindedKey();
    DWORD       getBindedKeyCode();

    void        extractAudioTitle();

private:
    int         id;
    QHBoxLayout *topLayout;
    s_BtnTools  tools;
    QPushButton *btnMain;
    FancySlider *volume;
    QString     pathSong;
    QString     btnTitle;
    QString     bindedKey;
    DWORD       bindedKeyCode;
};

#endif // KEY_H
