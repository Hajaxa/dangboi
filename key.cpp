#include "key.h"

Key::Key()
{
    id = 0;
    setTopLayout(new QHBoxLayout());
    setTools(new QPushButton(), new QPushButton(), new QPushButton());
    setPathSong("");
    setBindedKey("", 0);
    setBtn(new QPushButton(""));
    setDialVolume(new FancySlider());
}

Key::Key(int _id, QString &_pathSong) :
    id(_id)
{
    setTopLayout(new QHBoxLayout());
    setTools(new QPushButton(), new QPushButton(), new QPushButton());
    setPathSong(_pathSong);
    setBindedKey("No bind", 0);
    setBtn(new QPushButton(""));
    setDialVolume(new FancySlider());
}

Key::Key(int _id, QHBoxLayout *_topLayout, s_BtnTools _tools, QPushButton *btn, FancySlider *_volume, QString &_pathSong, QString &_bindedKey) :
    id(_id), topLayout(_topLayout), tools(_tools), btnMain(btn), volume(_volume), pathSong(_pathSong), bindedKey(_bindedKey)
{

}

Key::~Key()
{
    qDebug() << "Key destroyed";

    if (tools.btnDel != NULL && tools.btnSong != NULL && tools.btnBind != NULL)
    {
        delete this->tools.btnDel;
        delete this->tools.btnSong;
        delete this->tools.btnBind;
    }
    if (btnMain != NULL)
        delete this->btnMain;
    if (volume != NULL)
        delete this->volume;
    if (topLayout != NULL)
        delete this->topLayout;
}


/* *** SETTERS *** */

void        Key::setId(int _id)
{
    id = _id;
}

void        Key::setTopLayout(QHBoxLayout *_topLayout)
{
    topLayout = _topLayout;
}

void        Key::setTools(QPushButton *del, QPushButton *song, QPushButton *bind)       // ! SET ICON ON BUTTONS ! //
{
    del->setText(""); del->setDefault(false); del->setAutoDefault(false); del->setMinimumSize(24, 24); del->setMaximumSize(64, 64);
    song->setText(""); song->setDefault(false); song->setAutoDefault(false); song->setMinimumSize(24, 24); song->setMaximumSize(64, 64);
    bind->setText(""); bind->setDefault(false); bind->setAutoDefault(false); bind->setMinimumSize(24, 24); bind->setMaximumSize(64, 64);

    del->setIcon(QIcon(":/icon/trash.png"));
    song->setIcon(QIcon(":/icon/song.png"));
    bind->setIcon(QIcon(":/icon/bind_2.png"));
    //del->setIconSize(QSize(16, 16));
/*
    del->setStyleSheet("QPushButton { border-image: url(:/icon/trash.png);} "
                       "QPushButton:hover { border-image: url(:/icon/trash.png);}");

    song->setStyleSheet("QPushButton { border-image: url(:/icon/song.png);} "
                       "QPushButton:hover { border-image: url(:/icon/song.png);}");

    bind->setStyleSheet("QPushButton { border-image: url(:/icon/bind_2.png);} "
                       "QPushButton:hover { border-image: url(:/icon/bind_2.png);}");
*/
    //song->setIcon(QIcon(":/icon/song.png")); del->setIconSize(QSize(16, 16));

    qDebug() << del->icon().name();
    qDebug() << song->icon().name();


    tools = {del, song, bind};
}

void        Key::setBtn(QPushButton *btn)
{
    btn->setMinimumSize(100, 250);
    btn->setText(this->btnTitle + "\n(" + this->bindedKey + ")");
    btnMain = btn;
}

void        Key::setDialVolume(FancySlider *_volume)
{
    volume = _volume;
    volume->setOrientation(Qt::Horizontal);
    volume->setMaximum(100);
    volume->setMinimum(0);
    volume->setValue(100);
    volume->setSingleStep(5);
    volume->setPageStep(20);

    volume->setStyleSheet("QSlider::groove:horizontal {border: 1px solid #bbb; background: white; height: 10px; border-radius: 4px;} "
                          "QSlider::sub-page:horizontal {background: qlineargradient(x1: 0, y1: 0,    x2: 0, y2: 1, stop: 0 #66e, stop: 1 #bbf);"
                          "background: qlineargradient(x1: 0, y1: 0.2, x2: 1, y2: 1, stop: 0 #98F49E, stop: 1 #007208); border: 1px solid #777; height: 10px; border-radius: 4px;}"
                          "QSlider::add-page:horizontal {background: #fff; border: 1px solid #777; height: 10px; border-radius: 4px;}"
                          "QSlider::handle:horizontal {background: qlineargradient(x1:0, y1:0, x2:1, y2:1, stop:0 #eee, stop:1 #ccc); border: 1px solid #777; width: 13px;"
                          "margin-top: -2px; margin-bottom: -2px; border-radius: 4px;} "
                          "QSlider::handle:horizontal:hover {background: qlineargradient(x1:0, y1:0, x2:1, y2:1, stop:0 #fff, stop:1 #ddd);border: 1px solid #444;border-radius: 4px;}"
                          "QSlider::sub-page:horizontal:disabled {background: #bbb; border-color: #999;}"
                          "QSlider::add-page:horizontal:disabled {background: #eee;border-color: #999;}"
                          "QSlider::handle:horizontal:disabled {background: #eee;border: 1px solid #aaa;border-radius: 4px;}");
}

void        Key::setPathSong(QString path)
{
    pathSong = path;
    this->extractAudioTitle();
}

void        Key::setBindedKey(QString _bindedKey, DWORD _bindedKeyCode)
{
    bindedKey       = _bindedKey;
    bindedKeyCode   = _bindedKeyCode;
}

void        Key::setVolume(int vol)
{
    this->volume->setValue(vol);
}

void        Key::setBtnStyle(QString iconPath)
{
    QString     iconHover;
    QString     style;
    QStringList list = iconPath.split('.');
    int         i = list.count() - 2;


    if (i >= 0 && i + 1 < list.count())
        iconHover = list[i] + "_hover." + list[i + 1];

    style = "QPushButton { border-image: url(" + iconPath + ");} QPushButton:hover { border-image: url(" + iconHover + ");}";
    // "QPushButton { border-image: url(:/icon/piano_key2.png);} QPushButton:hover { border-image: url(:/icon/piano_key2.png);}";

    this->btnMain->setStyleSheet(style);
}



/* *** GETTERS *** */

int         Key::getId()
{
    return (id);
}

QHBoxLayout *Key::getTopLayout()
{
    return (topLayout);
}

s_BtnTools  Key::getTools()
{
    return(tools);
}

QPushButton *Key::getBtn()
{
    return(btnMain);
}

FancySlider       *Key::getDial()
{
    return (volume);
}

QString     Key::getPathSong()
{
    return (pathSong);
}

QString     Key::getBindedKey()
{
    return (bindedKey);
}

DWORD       Key::getBindedKeyCode()
{
    return (bindedKeyCode);
}


/* *** METHODS *** */

void        Key::extractAudioTitle()
{
    QString     title;
    std::string temp = pathSong.toStdString();
    int         pos_start = 0;
    int         pos_end = pathSong.size();

    pos_start = temp.find_last_of("/\\");
    pos_end = temp.find_last_of(".");
    if (pos_start < 0 || pos_end < 0)
    {
        this->btnTitle = "";
        return ;
    }

    title = temp.substr(pos_start + 1, pos_end - pos_start - 1).c_str();
    qDebug() << "Key::extractAudioTitle : " << title;
    this->btnTitle = title;
}
