#ifndef ENTERKEY_H
#define ENTERKEY_H

#include <QLineEdit>

class EnterKey : public QLineEdit
{
    Q_OBJECT
public:
    explicit EnterKey(QWidget *parent = 0);

protected:
    void    keyPressEvent(QKeyEvent *event);
    void    keyReleaseEvent(QKeyEvent *event);
};

#endif // ENTERKEY_H
