#include "enterkey.h"
#include <QKeyEvent>

#include <QDebug>

EnterKey::EnterKey(QWidget *parent) :
    QLineEdit(parent)
{
}

void EnterKey::keyPressEvent(QKeyEvent *event)
{
    qDebug() << "dangboi";
  // Quand le widget est désactivé on n'accepte pas le changement de touche
  if (!isEnabled())
  {
    QLineEdit::keyPressEvent(event);
    return;
  }

  // Filtre l'entrée normale du QLineEdit
  event->accept(); // edit: au lieu de e->ignore();

  // Si c'est un caractère imprimable
  if(!event->text().isEmpty())
  {
    QKeySequence sequence(event->key() | event->modifiers());
    setText(sequence.toString()); // Emettra le signal textChanged(QString)
  }
}

void EnterKey::keyReleaseEvent(QKeyEvent *event)
{
    if(event->key() == Qt::Key_Escape)
    {
        //qDebug() << "You released ESC";
    }
}
