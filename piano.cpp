#include "piano.h"
#include <QLabel>
#include <QSound>
#include <QSoundEffect>
#include <qglobal.h>

// Icons of main buttons
#define ICON_RIGHT  "./icon/key_solo_right.png"
#define ICON_LEFT   "./icon/key_solo_left.png"
#define ICON_DUO    "./icon/key_duo.png"

// Crosslink between Qt class and win callback
Piano       *pianoReference;
DWORD       lastKeyCode = 0;
QString     lastKeyName = "";


/* *** Keyboard Hook *** */

void                UpdateKeyState(BYTE *keystate, int keycode)
{
    keystate[keycode] = GetKeyState(keycode);
}

void                getPressedKeyName(WPARAM wParam, LPARAM lParam)
{
    // WPARAM is WM_KEYDOWN, WM_KEYUP, WM_SYSKEYDOWN, or WM_SYSKEYUP
        // LPARAM is the key information

        // Get the key information
        KBDLLHOOKSTRUCT cKey = *((KBDLLHOOKSTRUCT*)lParam);

        wchar_t         buffer[5];

        // Get the keyboard state
        BYTE            keyboard_state[256];
        GetKeyboardState(keyboard_state);
        UpdateKeyState(keyboard_state, VK_SHIFT);
        UpdateKeyState(keyboard_state, VK_CAPITAL);
        UpdateKeyState(keyboard_state, VK_CONTROL);
        UpdateKeyState(keyboard_state, VK_MENU);

        // Get keyboard layout
        HKL             keyboard_layout = GetKeyboardLayout(0);

        // Get the name
        char            lpszName[0x100] = {0};

        DWORD           dwMsg = 1;
        dwMsg += cKey.scanCode << 16;
        dwMsg += cKey.flags << 24;

        int             i = GetKeyNameText(dwMsg, (LPTSTR)lpszName, 255);

        i = i + 1; // ANTI WARNING
        // Try to convert the key info
        int             result = ToUnicodeEx(cKey.vkCode, cKey.scanCode, keyboard_state, buffer, 4, 0, keyboard_layout);
        buffer[4] = L'\0';
        result = result; // ANTI WARNING

        // Print the output
        if (wParam == WM_KEYDOWN)
            qDebug() << "key: "  << cKey.vkCode << " " << QString::fromUtf16((ushort*)buffer) << " " << QString::fromUtf16((ushort*)lpszName);

        lastKeyCode = cKey.vkCode;
        lastKeyName = QString::fromUtf16((ushort*)lpszName);

        if (cKey.vkCode != 32)  // Counter the multiple activation of the bind button, 32 = SPACE
            pianoReference->getBindDialog()->accept();
}

LRESULT CALLBACK LowLevelKeyboardProc(int nCode, WPARAM wParam, LPARAM lParam)
{
    /* *** */
    /*wchar_t buff[10];
    BYTE keyState[256] = {0};

    int result = ToUnicodeEx(PKBDLLHOOKSTRUCT(lParam)->vkCode, PKBDLLHOOKSTRUCT(lParam)->scanCode, keyState, buff, _countof(buff), 0, NULL);
    std::wcout << buff;*/
    if (nCode == HC_ACTION)
    {
        switch (wParam)
        {
            // Pass KeyDown/KeyUp messages for Qt class to logicize
            case WM_KEYDOWN:
                getPressedKeyName(wParam, lParam);
                pianoReference->keyDown(PKBDLLHOOKSTRUCT(lParam)->vkCode);
            break;
            case WM_KEYUP:
                pianoReference->keyUp(PKBDLLHOOKSTRUCT(lParam)->vkCode);
            break;
        }
    }
    return CallNextHookEx(NULL, nCode, wParam, lParam);
}



/* *** Piano Object *** */

Piano::Piano()
    : vecKeys(),
      sMapperDel(new QSignalMapper),
      sMapperSong(new QSignalMapper),
      sMapperBind(new QSignalMapper),
      sMapperBtn(new QSignalMapper),
      sMapperSlider(new QSignalMapper)
{
    connect(this->sMapperDel,       SIGNAL(mapped(int)), this, SLOT(btnDelClicked(int)));
    connect(this->sMapperSong,      SIGNAL(mapped(int)), this, SLOT(btnSongClicked(int)));
    connect(this->sMapperBind,      SIGNAL(mapped(int)), this, SLOT(btnBindClicked(int)));
    connect(this->sMapperBtn,       SIGNAL(mapped(int)), this, SLOT(btnMainClicked(int)));
    connect(this->sMapperSlider,    SIGNAL(mapped(int)), this, SLOT(btnSliderClicked(int)));

    QHBoxLayout *layout     = new QHBoxLayout();
    //EnterKey    *enterKey   = new EnterKey();

    this->bindDialog = new QDialog();
    //enterKey->setText("Press any key...");
    layout->addWidget(new QLabel("Press any key...\n(except SPACE, you're not worthy enough.)"));
    bindDialog->setLayout(layout);


    // Hooks setup
    pianoReference = this;
    bWinKey = false;

    // Install the low-level keyboard & mouse hooks
    hhkLowLevelKybd = SetWindowsHookEx(WH_KEYBOARD_LL, LowLevelKeyboardProc, 0, 0);
}

Piano::~Piano()
{
    UnhookWindowsHookEx(hhkLowLevelKybd);
    delete sMapperDel;
    delete sMapperSong;
    delete sMapperBind;
    delete sMapperBtn;
    delete sMapperSlider;
    delete bindDialog;

    for (int i = 0; i < this->vecKeys.size(); ++i)
    {
        if (this->vecKeys[i] != NULL)
            delete this->vecKeys[i];
    }
    this->vecKeys.clear();
}

int         Piano::findId()
{
    int     bigId = 0;

    for (int i = 0; i < this->vecKeys.size(); ++i)
    {
        if (vecKeys[i]->getId() > bigId)
            bigId = vecKeys[i]->getId();
    }
    return (bigId + 1);
}

Key         *Piano::findKeyById(int id)
{
    for (int i = 0; i < this->vecKeys.size(); ++i)
    {
        if (this->vecKeys[i]->getId() == id)
            return (this->vecKeys[i]);
    }
    return (NULL);
}

Key         *Piano::newKey(QString &pathSong)
{
    Key     *key = new Key(this->findId(), pathSong);

    sMapperDel->setMapping(key->getTools().btnDel, key->getId());
    sMapperSong->setMapping(key->getTools().btnSong, key->getId());
    sMapperBind->setMapping(key->getTools().btnBind,key->getId());
    sMapperBtn->setMapping(key->getBtn(), key->getId());
    sMapperSlider->setMapping(key->getDial(), key->getId());

    connect(key->getTools().btnDel,     SIGNAL(clicked(bool)),      this->sMapperDel, SLOT(map()));
    connect(key->getTools().btnSong,    SIGNAL(clicked(bool)),      this->sMapperSong, SLOT(map()));
    connect(key->getTools().btnBind,    SIGNAL(clicked(bool)),      this->sMapperBind, SLOT(map()));
    connect(key->getBtn(),              SIGNAL(clicked(bool)),      this->sMapperBtn, SLOT(map()));
    connect(key->getDial(),             SIGNAL(sliderMoved(int)),   this->sMapperSlider, SLOT(map()));

    vecKeys.push_back(key);
    return (key);
}

void        Piano::hideTools(bool hide)
{
    for (int i = 0; i < this->vecKeys.size(); ++i)
    {
        vecKeys[i]->getTools().btnDel->setHidden(hide);
        vecKeys[i]->getTools().btnSong->setHidden(hide);
        vecKeys[i]->getTools().btnBind->setHidden(hide);
        vecKeys[i]->getDial()->setHidden(hide);
    }
}

void        Piano::refreshKeyStyle()
{
    for (int i = 0; i < this->vecKeys.size(); ++i)
    {
        if (i == 0)
            this->vecKeys[i]->setBtnStyle(ICON_RIGHT);
        else if (i + 1 >= this->vecKeys.size())
            this->vecKeys[i]->setBtnStyle(ICON_LEFT);
        else
        {
            if (i - 1 >= 0 && this->vecKeys[i - 1]->getBtn()->styleSheet().contains(ICON_RIGHT))
                this->vecKeys[i]->setBtnStyle(ICON_DUO);
            else if (i - 1 >= 0 && this->vecKeys[i - 1]->getBtn()->styleSheet().contains(ICON_LEFT))
                this->vecKeys[i]->setBtnStyle(ICON_RIGHT);
            else
            {
                qsrand(qrand());
                int result = (qrand() % ((20 + 1) - 10) + 10);
                qDebug() << "refreshKeyStyle: " << result;
                if (result % 2 == 0 || i + 2 >= this->vecKeys.size())
                    this->vecKeys[i]->setBtnStyle(ICON_DUO);
                else
                    this->vecKeys[i]->setBtnStyle(ICON_LEFT);
            }
        }
    }
}


QDialog     *Piano::getBindDialog()
{
    return (this->bindDialog);
}

QString     Piano::getComposition()
{
    QString compo;

    for (int i = 0; i < this->vecKeys.size(); ++i)
    {
        Key *key = this->vecKeys[i];
        compo += key->getPathSong() + ";" + QString::number(key->getBindedKeyCode()) + ";" + key->getBindedKey() + ";" + QString::number(key->getDial()->value()) + "\n";
    }
    return (compo);
}







/* *** HOOK *** */

void        Piano::keyDown(DWORD key)
{
    // This section gets launched for every key pressed ever
    // Filter out the keys we are interested
    // Launch doMultimedia everytime key of interest is pressed
    //      Otherwise it'll still launch even if non-interested key is pressed
    //      Also will result in massive trigger spam when right keys are pressed
    //      By keeping it within the parenthesis, doMultimedia will fire on that specific key repeat
    //      Otherwise it'll spam on both win and key repeat, meaning it'll fire 30+ a second if outside brackets
    if(key == VK_LWIN || key == VK_RWIN)
    {
        bWinKey = true;
        //doMultimedia(key);
    }

    for (int i = 0; i < this->vecKeys.size(); ++i)
    {
        if (key == this->vecKeys[i]->getBindedKeyCode())
            this->btnMainClicked(this->vecKeys[i]->getId());
    }
}

void        Piano::keyUp(DWORD key)
{
    // Remove the bools
    if(key == VK_LWIN || key == VK_RWIN)
        bWinKey = false;
}






/* *** PRIVATE SLOTS *** */

void        Piano::btnDelClicked(int id)
{
    Key     *key = this->findKeyById(id);

    if (key == NULL)
        return ;

    for (int i = 0; i < this->vecKeys.size(); ++i)
    {
        if (this->vecKeys[i]->getId() == id)
        {
            this->vecKeys.remove(i);
            break;
        }
    }
    delete key;
    qDebug() << "emitting key deleted for key " << id;
    emit this->keyDeleted(id);
}

void        Piano::btnSongClicked(int id)
{
    Key     *key = this->findKeyById(id);

    if (key == NULL)
        return ;

    QString fileName = QFileDialog::getOpenFileName(this, tr("Change sound"), "./", tr("Audio files (*.mp3 *.ogg *.wav)"));

    if (fileName.isEmpty() || fileName == NULL)
        return ;
    else
    {
        key->setPathSong(fileName);
        key->setBtn(key->getBtn());
    }
}

void        Piano::btnBindClicked(int id)
{
    Key     *key = this->findKeyById(id);

    if (key == NULL)
        return ;

    if (bindDialog->exec() == QDialog::Accepted)
    {
        qDebug() << "Bind: " << lastKeyName;
        key->setBindedKey(lastKeyName, lastKeyCode);
        key->setBtn(key->getBtn());
    }
}

void        Piano::btnMainClicked(int id)
{
    Key     *key = this->findKeyById(id);

    if (key == NULL)
        return ;

    this->playSong(key->getPathSong(), key->getDial()->value());
}

void        Piano::btnSliderClicked(int id)
{
    Key     *key = this->findKeyById(id);

    if (key == NULL)
        return ;
}

void        Piano::playSong(QString pathSong, int volume)
{
    QSoundEffect  *sound = new QSoundEffect(this);

    sound->setSource(QUrl::fromLocalFile(pathSong));
    sound->setVolume((double)volume / 100);
    if (sound->status() == QSoundEffect::Ready)
        sound->play();
    qDebug() << "Playing song: " << pathSong << " at volume " << (double)volume / 100;
}








/*void Piano::pressKey(DWORD vkKeyCode)
{
    INPUT Input;
    // Set up a generic keyboard event.
    Input.type = INPUT_KEYBOARD;
    Input.ki.wScan = 0;
    Input.ki.time = 0;
    Input.ki.dwExtraInfo = 0;
    Input.ki.dwFlags = 0;

    Input.ki.wVk = vkKeyCode;
    SendInput(1, &Input, sizeof(INPUT));
}

void Piano::doMultimedia(DWORD vkKeyCode)
{
    // Check if any win key is being pressed first
    if(bWinKey) return;

    // Play / Pause
    if (vkKeyCode == VK_SPACE)
        pressKey(VK_MEDIA_PLAY_PAUSE);
    // Next Track
    else if (vkKeyCode == VK_F10)
        pressKey(VK_MEDIA_NEXT_TRACK);
    // Lower Volume
    else if (vkKeyCode == VK_DOWN)
        pressKey(VK_VOLUME_DOWN);
    // Increase Volume
    else if (vkKeyCode == VK_UP)
        pressKey(VK_VOLUME_UP);
    // Mute
    else if (vkKeyCode == VK_END)
        pressKey(VK_VOLUME_MUTE);
}*/

