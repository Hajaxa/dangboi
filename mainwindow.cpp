#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QGridLayout>

#define DEFAULT_PATH    "./autosave.boi"
#define AUTOSAVE_DELAY  30000

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    this->clearLayout(this->ui->hLayoutPiano);
    this->setLayoutAddSong();

    this->piano = new Piano();
    this->loadPiano(DEFAULT_PATH);

    this->timerSave = new QTimer();
    connect(timerSave, SIGNAL(timeout()), this, SLOT(autosave()));
    this->timerSave->start(AUTOSAVE_DELAY);

    //QGridLayout *grid = (QGridLayout*)this->ui->centralWidget->layout();
    //grid->addWidget(new QPushButton(""), 0, 1);
    //connect(this->ui->btnAddSong, SIGNAL(released()), this, SLOT(on_actionAdd_triggered()));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void        MainWindow::clearLayout(QLayout *layout)
{
    for (int i = 0; i < layout->count(); ++i)
    {
        QWidget *child_widget = NULL;
        QLayout *child_layout = NULL;

        try {
            child_layout = layout->itemAt(i)->layout();
            child_widget = layout->itemAt(i)->widget();

            if (child_layout == NULL && child_widget != NULL)
                throw 1;    // Ain't layout but is widget
            else if (child_layout != NULL && child_widget == NULL)
                throw 2;    // Is layout but ain't widget
            else
                throw 3;    // Unhandled exception
        }
        catch (int error)   {
            if (error == 1) {
                qDebug() << "clearLayout: deleting " << child_widget->metaObject()->className();
                delete child_widget;
                i = -1;     }
            else if (error == 2)    {
                this->clearLayout(child_layout);
                qDebug() << "clearLayout: deleting " << child_layout->metaObject()->className();
                delete child_layout;
                i = -1;             }
            else
                qDebug() << "clearLayout: unhandled exception !";}
    }
}

Key         *MainWindow::addKeyPiano(QString &pathSong)
{
    Key         *key = this->piano->newKey(pathSong);
    QVBoxLayout *newLayout = new QVBoxLayout();
    QHBoxLayout *volumeLayout = new QHBoxLayout();

    /* ** Delete add song button ** */
    this->clearLayout(this->layoutAddSong);

    /* ** Top Tools buttons ** */
    key->getTopLayout()->addWidget(key->getTools().btnDel);
    key->getTopLayout()->addWidget(key->getTools().btnSong);
    key->getTopLayout()->addWidget(key->getTools().btnBind);
    key->getTopLayout()->addSpacerItem(new QSpacerItem(40, 0, QSizePolicy::Expanding));
    newLayout->addLayout(key->getTopLayout());

    /* ** Main button ** */
    newLayout->addWidget(key->getBtn());

    /* ** Volume slider ** */
    volumeLayout->addWidget(key->getDial());
    volumeLayout->addSpacerItem(new QSpacerItem(20, 0, QSizePolicy::Minimum));
    newLayout->addLayout(volumeLayout);

    /* ** Set hidden ** */
    this->piano->hideTools(!this->ui->actionTools->isChecked());

    /* ** Features for clean delete of key and associated UI ** */
    this->mapLayout.insert(key->getId(), newLayout);                // Pair key ID with appropriate layout
    connect(this->piano, SIGNAL(keyDeleted(int)), this, SLOT(deleteKeyUI(int)));

    /* ** Refresh design of piano keyboard ** */
    this->piano->refreshKeyStyle();

    /* ** Add layout and replace add song button ** */
    this->ui->hLayoutPiano->addLayout(newLayout);
    this->setLayoutAddSong();

    return (key);
}

void        MainWindow::loadPiano(QString path)
{
    QFile   file(path);

    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
        QMessageBox::critical(this, "Error opening file", "Can't open file " + path);
    else
    {
        QTextStream     in(&file);
        bool            bogue = false;

        setUpdatesEnabled(false);

        if (this->piano != NULL)
            delete this->piano;
        this->piano = new Piano();
        this->clearLayout(this->ui->hLayoutPiano);
        this->setLayoutAddSong();

        while (!in.atEnd())
        {
            QString     line = in.readLine();
            QStringList list = line.split(';');
            Key         *key;

            if (list.size() == 4)
            {
                qDebug() << "dangboi load";
                QString path = list.at(0);
                key = this->addKeyPiano(path);
                key->setBindedKey(list.at(2), QString(list.at(1)).toLong());
                key->setVolume(list.at(3).toInt());
                key->setBtn(key->getBtn());
            }
            else
                bogue = true;
        }
        if (bogue)
            QMessageBox::warning(this, "Damaged file", "The piano file is damaged and some data couldn't be retrieved");

        setUpdatesEnabled(true);
    }
}

void        MainWindow::savePiano(QString path)
{
    QFile       file(path);

    if (!file.open(QIODevice::WriteOnly | QIODevice::Text | QIODevice::Truncate))
        qDebug() << "Failed to open " + file.fileName();
    else
    {
        QTextStream stream(&file);
        stream.flush();
        stream << this->piano->getComposition();
        file.close();
        qDebug() << this->piano->getComposition();
    }
}

void        MainWindow::setLayoutAddSong()
{
    QToolButton *btnAddSong = new QToolButton();
    QHBoxLayout *layoutLabel= new QHBoxLayout();
    this->layoutAddSong     = new QVBoxLayout();
    this->labelAddSong      = new QPushButton("Add a sound effect");


    this->labelAddSong->setMinimumSize(50, 24);
    this->labelAddSong->setMaximumSize(150, 24);
    this->labelAddSong->setFlat(true);
    this->labelAddSong->setDisabled(true);
    this->labelAddSong->setStyleSheet("QPushButton { color: rgb(0, 0, 0); }");
    this->labelAddSong->setHidden(!this->ui->actionTools->isChecked());
    layoutLabel->addWidget(this->labelAddSong);
    layoutLabel->addSpacerItem(new QSpacerItem(0, 25, QSizePolicy::Minimum, QSizePolicy::MinimumExpanding));

    btnAddSong->setMinimumSize(100, 250);
    btnAddSong->setIcon(QIcon(":/icon/add.png"));
    //btnAddSong->setStyleSheet("QToolButton { border-image: url(:/icon/add.png);} "
      //                 "QToolButton:hover { border-image: url(:/icon/trash.png);}");
    connect(btnAddSong, SIGNAL(released()), this, SLOT(on_actionAdd_triggered()));

    layoutAddSong->addLayout(layoutLabel);
    layoutAddSong->addWidget(btnAddSong);
    layoutAddSong->addSpacerItem(new QSpacerItem(0, 25, QSizePolicy::Minimum, QSizePolicy::MinimumExpanding));

    this->ui->hLayoutPiano->addLayout(this->layoutAddSong);
}








/* *** SLOTS *** */

void        MainWindow::on_actionSaveas_triggered()
{
    QString fileName = QFileDialog::getSaveFileName(this, tr("Save your dangboi piano"), "./", tr("Dangboi files (*.boi)"));

    if (fileName.isEmpty() || fileName == NULL)
        return ;
    else
        this->savePiano(fileName);
}

void        MainWindow::on_actionOpen_triggered()
{
    QString fileName = QFileDialog::getOpenFileName(this, tr("Open a dangboi file"), "./", tr("Dangboi files (*.boi)"));

    if (fileName.isEmpty() || fileName == NULL)
        return;
    else
        this->loadPiano(fileName);
}

void        MainWindow::autosave()
{
    this->savePiano(DEFAULT_PATH);
}

void        MainWindow::on_actionAdd_triggered()
{
    QString fileName = QFileDialog::getOpenFileName(this, tr("Select a sound"), "./", tr("Audio files (*.mp3 *.ogg *.wav)"));

    if (fileName.isEmpty() || fileName == NULL)
        return ;
    else
        this->addKeyPiano(fileName);
}

void        MainWindow::on_actionTools_triggered(bool checked)
{
    this->piano->hideTools(!checked);
    this->labelAddSong->setHidden(!checked);
}

void        MainWindow::deleteKeyUI(int id)
{
    QMap<int, QLayout*>::const_iterator i = this->mapLayout.find(id);

    if (i != this->mapLayout.end())
    {
        this->clearLayout(i.value());
        this->mapLayout.remove(id);
        this->resize(this->size().width() - 100, this->size().height());
        this->piano->refreshKeyStyle();
        qDebug() << "signal received: deleting ui for key " << id;
    }
}
