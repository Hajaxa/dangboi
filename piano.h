#ifndef PIANO_H
#define PIANO_H

#include <QObject>
#include <QVector>
#include <QSignalMapper>
#include <QFileDialog>
#include <QMessageBox>
#include <QKeyEvent>

#include "key.h"
#include "enterkey.h"

class Piano : public QWidget
{
    Q_OBJECT

public:
    Piano();
    ~Piano();

    int                     findId();
    Key                     *findKeyById(int id);
    Key                     *newKey(QString &pathSong);

    void                    hideTools(bool hide);
    void                    refreshKeyStyle();

    QDialog                 *getBindDialog();
    QString                 getComposition();

    void                    keyDown(DWORD key);
    void                    keyUp(DWORD key);
    //static void             pressKey(DWORD vkKeyCode);

signals:
    void                    keyDeleted(int id);

private slots:
    void                    btnDelClicked(int);                         // Called when a delete button is triggered
    void                    btnSongClicked(int);                        // Called when a song button is triggered
    void                    btnBindClicked(int);                        // Called when a bind button is triggered
    void                    btnMainClicked(int);                        // Called when a main button is triggered
    void                    btnSliderClicked(int);                      // Called when a slider is triggered

    //void                  doMultimedia(DWORD vkKeyCode);
    void                    playSong(QString pathSong, int volume);

private:
    QVector<Key*>           vecKeys;
    QSignalMapper           *sMapperDel;                                // Mapper for delete buttons
    QSignalMapper           *sMapperSong;                               // Mapper for song buttons
    QSignalMapper           *sMapperBind;                               // Mapper for bind buttons
    QSignalMapper           *sMapperBtn;                                // Mapper for main buttons
    QSignalMapper           *sMapperSlider;                             // Mapper for volume sliders

    QDialog                 *bindDialog;
    HHOOK                   hhkLowLevelKybd;
    bool                    bWinKey;
};

#endif // PIANO_H
