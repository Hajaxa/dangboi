#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTextStream>
#include <QStringList>
#include <QTimer>
#include <QToolButton>
#include <QMap>
#include <cstdlib>

#include "piano.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void                clearLayout(QLayout *layout);
    Key                 *addKeyPiano(QString &pathSong);

    void                loadPiano(QString path);
    void                savePiano(QString path);

    void                setLayoutAddSong();

private slots:
    void                on_actionAdd_triggered();
    void                on_actionTools_triggered(bool checked);
    void                on_actionSaveas_triggered();
    void                on_actionOpen_triggered();
    void                autosave();
    void                deleteKeyUI(int id);

private:
    Ui::MainWindow      *ui;
    Piano               *piano;
    QMap<int, QLayout*> mapLayout;                              // Layout per key ID
    QTimer              *timerSave;
    QVBoxLayout         *layoutAddSong;
    QPushButton         *labelAddSong;
};

#endif // MAINWINDOW_H
